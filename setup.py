from setuptools import setup

from automatic_jupyter_kernel.install_kernelspec import install

install()  # this library works by introducing side effects while installing

setup(
    name="automatic-jupyter_kernel",
    version="1.0",
    description="Automatic Python Jupyter kernel ",
    packages=["automatic_jupyter_kernel"],
    python_requires=">=3.9",
    install_requires=["jupyter-client"],
)