## inspired by https://github.com/pathbird/poetry-kernel
import subprocess
import sys
import signal
from pathlib import Path
from typing import Optional


def is_already_installed(python_venv_path: Optional[Path]):
    """Use a blank file to remember if ipykernel is already install on the venv"""
    touch_path = python_venv_path.parent if python_venv_path else Path(".").resolve()
    if (touch_path / ".is_installed.txt").exists():
        return True
    return False


def confirm_install(python_venv_path: Optional[Path]):
    """Use a blank file to remember if ipykernel is already install on the venv"""
    touch_path = python_venv_path.parent if python_venv_path else Path(".").resolve()
    (touch_path / ".is_installed.txt").touch()


def find_in_path(file_name: str):
    """
    Try to find a venv with the given file_name in the current repertory or its parent
    (as notebooks are sometimes in a /notebook folder)
    """
    cwd = Path(".").resolve()
    candidate_dirs = [cwd, *cwd.parents]
    for dirs in candidate_dirs[:2]:  # only sibling and oncle venv are accepted
        venv_dir = dirs / file_name
        if venv_dir.exists():
            return venv_dir
    return None


def install_requirements(python_venv_path: Path):
    """
    Install ipykernel (necessary for notebooks) with the given python interpreter
    """
    ipykernel_cmd = [f"{python_venv_path}", "-m", "pip", "install", "ipykernel"]
    process = subprocess.Popen(ipykernel_cmd)
    process.wait()


def main():
    """
    Will search for a close by venv. If not found, he will create it himself,
    and add ipykernel to the installed packages.
    The later times it is used, it will just automatically use the right path for the environnement
    """
    venv_path = find_in_path(".venv")
    python_venv_path = venv_path / "bin" / "python" if venv_path else None
    poetry_path = find_in_path("pyproject.toml")
    if not is_already_installed(python_venv_path):
        if python_venv_path:
            install_requirements(python_venv_path)
        elif poetry_path:
            poetry_ipykernel_cmd = ["poetry", "add", "ipykernel"]
            process = subprocess.Popen(poetry_ipykernel_cmd)
            process.wait()
        else:
            venv_cmd = [f"python", "-m", "venv", ".venv"]
            subprocess.Popen(venv_cmd)
            python_venv_path = Path(".venv").resolve() / "bin" / "python"
            install_requirements(python_venv_path)
        confirm_install(python_venv_path)

    python_commands = [str(python_venv_path)] if python_venv_path else ["poetry", "run", "python"]

    # the goal here is to replace "/usr/bin/python3 -m automatic_jupyter_kernel -f "{connection_file}"
    # with "./venv/bin/python3 -m ipykernel_launcher -f {connection_file}"
    # or "poetry run -m ipykernel_launcher -f {connection_file}"
    # (ipykernel_launcher should now be installed)

    f, connection_file = sys.argv[1:3]
    cmd = [
        *python_commands, "-m", "ipykernel_launcher",
        f, connection_file,
    ]
    proc = subprocess.Popen(cmd)

    forward_signals = set(signal.Signals) - {signal.SIGKILL, signal.SIGSTOP}

    def handle_signal(sig, _frame):
        proc.send_signal(sig)

    for sig in forward_signals:
        signal.signal(sig, handle_signal)

    exit_code = proc.wait()
    if exit_code == 0:
        print("ipykernel_launcher exited", file=sys.stderr)
    else:
        print("ipykernel_launcher exited with error code:", exit_code, file=sys.stderr)


if __name__ == "__main__":
    main()