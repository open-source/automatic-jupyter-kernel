import json
import os.path
import tempfile
from pathlib import Path

PATH_TO_ROOT = Path(".").parent.resolve()


def _write_kernelspec(kernelspec_directory):
    """
    Write a special kernel which, instead of calling the `ipykernel_launcher` of the wanted right environment,
    will call automatic_jupyter_kernel which will :
    - find an appropriate environment nearby
    - install `ipykernel_launcher` on it
    - use it to launch the command
    """
    spec = {
        "argv": [
            str(PATH_TO_ROOT / ".venv/bin/python"), "-m", "automatic_jupyter_kernel",
            "-f", "{connection_file}"
        ],
        "display_name": "automatic_kernel",
        "language": "python",
    }
    with open(os.path.join(kernelspec_directory, "kernel.json"), "w") as fp:
        json.dump(spec, fp, indent=4)


def install():
    # Make this import inside the install function because this file is used
    # during package install and we don't necessarily have jupyter_client
    # installed yet
    from jupyter_client.kernelspec import KernelSpecManager

    manager = KernelSpecManager()
    with tempfile.TemporaryDirectory() as tmpdir:
        _write_kernelspec(tmpdir)
        manager.install_kernel_spec(tmpdir, kernel_name="automatic_kernel", user=True)
